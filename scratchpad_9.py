# # add = lambda a, b: a + b

# # print(add(10, 20))


# # nums = [1, 2, 3, 4, 5, 6, 7, 8]

# # doubles = []

# # for num in nums:
# #     doubles.append(num ** 2)

# # doubles = [num ** 2 for num in nums]

# # doubles = map(lambda num: num**2, nums)

# # print(list(doubles))

# # nums = [1, 2, 3, 4, 5, 6, 7, 8]

# # evens = []

# # for num in nums:
# #     if num % 2 == 0:
# #         evens.append(num)

# # evens = [num for num in nums if num % 2 == 0]

# # evens = filter(lambda num: num % 2 == 0, nums)

# # print(list(evens))

# # names = ["John", "Jack", "James", "Desmond", "Charlie", "Jacob"]

# # result = map(
# #     lambda name: f"The one who wins is {name}",
# #     filter(lambda name: len(name) < 5, names),
# # )

# # print(list(result))


# # people = ["John", "Jack", "James", "Jason", "Kane", "Jacob"]

# # False

# # def hello:
# #     print("hello world")


# # def print_in_color(text, color):
# #     colors = (
# #         "red",
# #         "yellow",
# #         "green",
# #         "blue",
# #         "orange",
# #         "purple",
# #         "black",
# #         "white",
# #         "pink",
# #     )

# #     if type(text) != str or type(color) != str:
# #         raise TypeError(f"Params `color` and `text` have to be of type str.")

# #     if color not in colors:
# #         raise ValueError(f"Sorry, {color} is not a supported color.")

# #     print(f"{text} :: printed in {color}")


# def print_in_color(text, color):
#     colors = (
#         "red",
#         "yellow",
#         "green",
#         "blue",
#         "orange",
#         "purple",
#         "black",
#         "white",
#         "pink",
#     )

#     if type(text) != str or type(color) != str:
#         raise TypeError(f"Params `color` and `text` have to be of type str.")

#     if color not in colors:
#         raise ValueError(f"Sorry, {color} is not a supported color.")

#     return f"{text} :: printed in {color}"


# try:
#     result = print_in_color("Hello World", "golden")
# except Exception as err:
#     print("Something went wrong")
#     print(err)
#     print()
# else:
#     print(result)
# finally:
#     print("THIS WILL ALWAYS RUN")
#     print()


# # except ValueError:
# #     print("Something went wrong")
# #     print()

# # except ValueError as err:
# #     print("Something went wrong")
# #     print()

# # except:
# #     print("Something went wrong")
# #     print()


# # except Exception as err:
# #     print("Something went wrong")
# #     print(err)
# #     print()

# # except (TypeError, ValueError) as err:
# #     print("Something went wrong")
# #     print(err)
# #     print()

# print("Hello World. This will run in the end.")


# Garbage Collector
# a = [1,2,3]
# new
# delete

# import random as r


# def random():
#     print("This is some random text")


# random()

# print(r.randint(10, 100))
# print(r.random())


# import random
# from random import randint, random, choice, shuffle
from random import randint as ri, random as r

# from random import *

# print(random.randint(0, 10))

print(ri(0, 10))
print(r())
