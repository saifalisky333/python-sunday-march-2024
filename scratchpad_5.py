# # # # langs = ["Python", "JavaScript", "Rust", "Clojure", "LISP", "Scheme", "Elm", "Haskell"]

# # # # print(langs[3:][::-1])


# # # langs = ["Python", "JavaScript", "Scheme", "Elm", "Haskell"]

# # # upper_langs = [lang.upper() for lang in langs]

# # # # upper_langs = []

# # # # for lang in langs:
# # # #     upper_langs.append(lang.upper())

# # # print(upper_langs)


# # # nums = [1, 2, 3, 4, 5, 6]

# # # doubles = [n * 2 for n in nums]

# # # # doubles = []

# # # # for num in nums:
# # # #     doubles.append(num * 2)

# # # print(doubles)


# # # nums = list(range(1, 21))

# # # evens = [num for num in nums if num % 2 == 0]

# # # # evens = []

# # # # for num in nums:
# # # #     if num % 2 == 0:
# # # #         evens.append(num)

# # # print(evens)


# # nums = list(range(1, 11))

# # result = [n * 2 if n % 2 == 0 else n / 2 for n in nums]

# # # result = []

# # for num in nums:
# #     if num % 2 == 0:
# #         result.append(num * 2)
# #     else:
# #         result.append(num / 2)

# # print(result)


# # message = "hello world this is something"


# nested_list = [[1, 2, 3], [4, 5, 6], [7, 8, 9]]

# for lst in nested_list:
#     for num in lst:
#         print(num)


# product1 = ["iPhone 15", "Apple", "Some description....", 500, 100000, "https://..."]
# # product[0]

# product2 = {
#     "name": "iPhone 15",
#     "brand": "Apple",
#     "description": "Some description...",
#     "price": 100000,
#     "in_stock": 500,
#     "image_url": "https://....",
#     10: True,
# }


# song = {
#     "title": "Song name #1",
#     "album": "Something",
#     "artists": ["artist #1", "artist #2"],
# }


# a = ["hello", "world", "test", "message"]

# for item in a:
#     print(item)

product = {
    "name": "iPhone 15",
    "brand": "Apple",
    "description": "Some description...",
    "price": 100000,
    "in_stock": 500,
    "image_url": "https://....",
}

# for key in product.keys():
#     print(key)

# for value in product.values():
#     print(value)


# for key in product.keys():
#     print(key, product[key])

# for key in product:
#     print(key, product[key])

# for item in product.items():
#     # print(item)
#     print(item[0], item[1])


for k, v in product.items():
    print(k, v)
